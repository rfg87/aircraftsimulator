package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Economy;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class EconomyTests {

	@Test
	public void testUpgrade() throws PassengerException {
		// Create a dummy passengers and then upgrade their fare class
		Economy dummy = new Economy(1, 1);
		Passenger dummyupgrade = dummy.upgrade();
		
		// Check if the upgraded passenger has indeed been upgraded and is the same as the original passenger
		assertTrue(dummyupgrade instanceof Premium && dummy.toString().equals(dummyupgrade.toString()));		
	}

	@Test
	public void testEconomyWorking() throws PassengerException {
		// Correct Arguments
		new Premium(1, 1);
	}
	
	@Test(expected = PassengerException.class)
	public void testEconomyInvalidBookingTime() throws PassengerException {
		// Invalid Booking Time (BT < 0)
		new Premium(-1, 1);
	}	
	
	@Test(expected = PassengerException.class)
	public void testEconomyInvalidDepartureTime() throws PassengerException {
		// Invalid Departure Time (DT <= 0)
		new Economy(1, 0);
	}
	
	@Test(expected = PassengerException.class)
	public void testEconomyDepartureBeforeBooking() throws PassengerException {
		// Departure Time < Booking Time
		new Economy(2, 1);
	}
	
	@Test
	public void testNoSeatsMsg() {
		assertTrue(true);
	}

}
