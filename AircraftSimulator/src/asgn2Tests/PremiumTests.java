package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class PremiumTests {

	@Test
	public void testUpgrade() throws PassengerException {
		// Create a dummy passengers and then upgrade their fare class
		Premium dummy = new Premium(1, 1);
		Passenger dummyupgrade = dummy.upgrade();
		
		// Check if the upgraded passenger has indeed been upgraded and is the same as the original passenger
		assertTrue(dummyupgrade instanceof Business && dummy.toString().equals(dummyupgrade.toString()));		
	}

	@Test
	public void testPremiumWorking() throws PassengerException {
		// Correct Arguments
		new Premium(1, 1);
	}
	
	@Test(expected = PassengerException.class)
	public void testPremiumInvalidBookingTime() throws PassengerException {
		// Invalid Booking Time (BT < 0)
		new Premium(-1, 1);
	}	
	
	@Test(expected = PassengerException.class)
	public void testPremiumInvalidDepartureTime() throws PassengerException {
		// Invalid Departure Time (DT <= 0)
		new Premium(1, 0);
	}
	
	@Test(expected = PassengerException.class)
	public void testPremiumDepartureBeforeBooking() throws PassengerException {
		// Departure Time < Booking Time
		new Premium(2, 1);
	}


	@Test
	public void testNoSeatsMsg() {
		assertTrue(true);
	}

	@Test
	public void testPremium() {
		assertTrue(true);
	}

}
