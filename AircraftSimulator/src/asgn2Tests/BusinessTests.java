package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

/**
 * @author Bradley Gray
 */

public class BusinessTests {
	@Test
	public void testUpgrade() throws PassengerException {
		// Create a dummy passengers and then upgrade their fare class
		Business dummy = new Business(1, 1);
		Passenger dummyupgrade = dummy.upgrade();
		
		// Check if the upgraded passenger has indeed been upgraded and is the same as the original passenger
		assertTrue(dummyupgrade instanceof First && dummy.toString().equals(dummyupgrade.toString()));		
	}

	@Test
	public void testBusinessWorking() throws PassengerException {
		// Correct Arguments
		new Business(1, 1);
	}
	
	@Test(expected = PassengerException.class)
	public void testBusinessInvalidBookingTime() throws PassengerException {
		// Invalid Booking Time (BT < 0)
		new Business(-1, 1);
	}	
	
	@Test(expected = PassengerException.class)
	public void testBusinessInvalidDepartureTime() throws PassengerException {
		// Invalid Departure Time (DT <= 0)
		new Business(1, 0);
	}
	
	@Test(expected = PassengerException.class)
	public void testBusinessDepartureBeforeBooking() throws PassengerException {
		// Departure Time < Booking Time
		new Business(2, 1);
	}

	@Test
	public void testBusiness() {
		assertTrue(true);
	}
	
	@Test
	public void testNoSeatsMsg() {
		assertTrue(true);
	}

}
