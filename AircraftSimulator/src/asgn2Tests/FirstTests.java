package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.First;
import asgn2Passengers.PassengerException;

public class FirstTests {

	@Test
	public void testFirstWorking() throws PassengerException {
		// Correct Arguments
		new First(1, 1);
	}
	
	@Test(expected = PassengerException.class)
	public void testFirstInvalidBookingTime() throws PassengerException {
		// Invalid Booking Time (BT < 0)
		new First(-1, 1);
	}	
	
	@Test(expected = PassengerException.class)
	public void FirstInvalidDepartureTime() throws PassengerException {
		// Invalid Departure Time (DT <= 0)
		new First(1, 0);
	}
	
	@Test(expected = PassengerException.class)
	public void testFirstDepartureBeforeBooking() throws PassengerException {
		// Departure Time < Booking Time
		new First(2, 1);
	}
	
	@Test
	public void testPassengerWorking() throws PassengerException {
		// Correct Arguments
		new First(1, 2);
	}
	
	@Test(expected = PassengerException.class)
	public void testPassengerInvalidBooking() throws PassengerException {
		// Invalid Booking Time (BT < 0)
		new First(-1, 2);
	}
	
	@Test(expected = PassengerException.class)
	public void testPassengerInvalidDeparture() throws PassengerException {
		// Invalid Departure Time (DT <= 0)
		new First(1, 0);
	}
	
	@Test(expected = PassengerException.class)
	public void testPassengerDepartureTimeBeforeBookingTime() throws PassengerException {
		// Departure Time < Booking Time
		new First(3, 2);
	}
	
	@Test
	public void testCancelSeatWorking() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to cancel seat.
		dummy.confirmSeat(7, 10);
		dummy.cancelSeat(9);
		
		// Check if passenger is new and booking time has been set
		assertTrue(dummy.isNew() && dummy.getBookingTime() == 9);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatPNotConfirmed() throws PassengerException {
		First dummy = new First(5, 10);
		// Don't confirm passenger
		dummy.cancelSeat(9);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatPInQueue() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to cancel seat.
		dummy.queuePassenger(7, 10);
		dummy.cancelSeat(9);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatPDeparted() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm and fly passenger before being able to cancel seat.
		dummy.confirmSeat(6, 10);
		dummy.flyPassenger(10);
		dummy.cancelSeat(9);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatpRefused() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue and refuse passenger before being able to cancel seat.
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(6);
		dummy.cancelSeat(7);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatInvalidCancellation() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to cancel seat.
		dummy.confirmSeat(7, 10);
		dummy.cancelSeat(-1);
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelSeatDepartureTimeEarlierCancellationTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to cancel seat.
		dummy.confirmSeat(7, 10);
		dummy.cancelSeat(11);
	}
	
	@Test
	public void testConfirmSeatWorking() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.confirmSeat(6, 10);
		// Check if passenger is confirmed and variables properly set
		assertTrue(dummy.isConfirmed() && dummy.getConfirmationTime() == 6 && dummy.getDepartureTime() == 10);
	}

	@Test(expected = PassengerException.class)
	public void testConfirmSeatAlreadyConfirmed() throws PassengerException {
		First dummy = new First(5, 10);
		
		// Confirm seat twice
		dummy.confirmSeat(6, 10);
		dummy.confirmSeat(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testConfirmSeatRefused() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue and refuse passenger before being able to confirm seat
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(6);
		dummy.confirmSeat(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testConfirmSeatDeparted() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm and fly passenger before being able to confirm seat
		dummy.confirmSeat(6, 10);
		dummy.flyPassenger(10);
		dummy.confirmSeat(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testConfirmSeatInvalidConfirmationTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.confirmSeat(-1, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testConfirmSeatDepartureTimeEarlierThanConfirmationTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.confirmSeat(11, 10);
	}
	
	@Test
	public void testFlyPassengerWorking() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to fly passenger
		dummy.confirmSeat(6, 10);
		dummy.flyPassenger(10);
		
		assertTrue(dummy.isFlown() && dummy.getDepartureTime() == 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengerNotConfirmed() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.flyPassenger(10);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengerQueue() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to fly passenger
		dummy.queuePassenger(6, 10);
		dummy.flyPassenger(10);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengerRefused() throws PassengerException {
		First dummy = new First(5, 10);

		// First must queue then refuse passenger before being able to fly passenger
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(6);
		dummy.flyPassenger(6);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengerAlreadyDeparted() throws PassengerException {
		First dummy = new First(5, 10);
		
		
		// First must confirm passenger before being able to fly passenger
		dummy.confirmSeat(6, 10);
		
		dummy.flyPassenger(10);
		dummy.flyPassenger(10);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengerInvalidDepartureTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to fly passenger
		dummy.confirmSeat(6, 10);
		
		dummy.flyPassenger(-1);
	}
	

	@Test
	public void testQueuePassengerWorking() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.queuePassenger(6, 10);
		
		assertTrue(dummy.isQueued() && dummy.getEnterQueueTime() == 6);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerAlreadyInQueue() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.queuePassenger(6, 10);
		dummy.queuePassenger(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerConfirmed() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to queue passenger
		dummy.confirmSeat(6, 10);
		dummy.queuePassenger(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerRefused() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must refuse passenger before being able to fly passenger
		dummy.refusePassenger(6);
		dummy.queuePassenger(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerDeparted() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm and fly passenger before being able to fly passenger
		dummy.confirmSeat(6, 10);
		dummy.flyPassenger(10);
		dummy.queuePassenger(6, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerInvalidQueueTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.queuePassenger(-1, 10);
	}
	
	@Test(expected = PassengerException.class)
	public void testQueuePassengerDepartureTimeEarlierThanQueueTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		dummy.queuePassenger(6, 5);
	}

	@Test
	public void testRefusePassengerWorking() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to refuse passenger
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(6);
		
		assertTrue(dummy.isRefused());
	}
	
	@Test(expected = PassengerException.class)
	public void testRefusePassengerIsConfirmed() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must confirm passenger before being able to refuse passenger
		dummy.confirmSeat(6, 10);
		dummy.refusePassenger(6);
	}
	
	@Test(expected = PassengerException.class)
	public void testRefusePassengerAlreadyRefused() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to refuse passenger
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(6);
		dummy.refusePassenger(6);
	}
	
	@Test(expected = PassengerException.class)
	public void testRefusePassengerDeparted() throws PassengerException {
		First dummy = new First(5, 10);

		// First must queue passenger before being able to fly passenger		
		dummy.confirmSeat(6, 10);
		dummy.flyPassenger(10);
		dummy.refusePassenger(7);
	}
	
	@Test(expected = PassengerException.class)
	public void testRefusePassengerInvalidRefusalTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to refuse passenger
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(-1);
	}
	
	@Test(expected = PassengerException.class)
	public void testRefusePassengerRefusalTimeEarlierThanBookingTime() throws PassengerException {
		First dummy = new First(5, 10);
		
		// First must queue passenger before being able to refuse passenger
		dummy.queuePassenger(6, 10);
		dummy.refusePassenger(4);
	}

	@Test
	public void testWasConfirmedTrue() throws PassengerException {
		First dummy = new First(1, 3);
		dummy.confirmSeat(2, 3);
		
		assertTrue(dummy.wasConfirmed());
	}
	
	@Test
	public void testWasConfirmedFalse() throws PassengerException {
		First dummy = new First(1, 3);
		
		assertTrue(!dummy.wasConfirmed());
	}

	@Test
	public void testWasQueuedTrue() throws PassengerException {
		First dummy = new First(1, 3);
		dummy.queuePassenger(2, 3);
		
		assertTrue(dummy.wasQueued());
	}
	
	@Test
	public void testWasQueuedFalse() throws PassengerException {
		First dummy = new First(1, 3);
		
		assertTrue(!dummy.wasQueued());
	}
	
	/**
	 * The following are all trivial tests and such do not need to be properly checked
	 */
	
	@Test
	public void testUpgrade() {
		assertTrue(true);
	}
	
	@Test
	public void testNoSeatsMsg() {
		assertTrue(true);
	}
	
	@Test
	public void testFirst() {
		assertTrue(true);
	}
	
	@Test
	public void testPassenger() {
		assertTrue(true);
	}
	
	@Test
	public void testGetBookingTime() {
		assertTrue(true);
	}

	@Test
	public void testGetConfirmationTime() {
		assertTrue(true);
	}

	@Test
	public void testGetDepartureTime() {
		assertTrue(true);
	}

	@Test
	public void testGetEnterQueueTime() {
		assertTrue(true);
	}

	@Test
	public void testGetExitQueueTime() {
		assertTrue(true);
	}

	@Test
	public void testGetPassID() {
		assertTrue(true);
	}

	@Test
	public void testIsConfirmed() {
		assertTrue(true);
	}

	@Test
	public void testIsFlown() {
		assertTrue(true);
	}

	@Test
	public void testIsNew() {
		assertTrue(true);
	}

	@Test
	public void testIsQueued() {
		assertTrue(true);
	}

	@Test
	public void testIsRefused() {
		assertTrue(true);
	}
	
	@Test
	public void testToString() {
		assertTrue(true);
	}

}