package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;
import asgn2Aircraft.*;
import asgn2Passengers.*;;

public class A380Tests {
	
	
	@Test
	public void testA380StringInt() throws AircraftException {
		A380 plane = new A380("QF11", 5);
		assertTrue(plane.equals(plane));
	}
	
	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidDepartureTimeTest() throws AircraftException {
		//departure time must be > 0
		new A380("QF11", -1);
	}
	
	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidFlightCode() throws AircraftException {
		//flight code cannot be null
		new A380(null, 5, 1, 2, 3, 4);
	}
	
	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidFirstSeatCapacity() throws AircraftException {
		//first class seat capacity must be >= 0
		new A380("QF11", 5, -1, 2, 3, 4);
	}

	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidBusinessSeatCapacity() throws AircraftException {
		//business class seat capacity must by >= 0
		new A380("QF11", 5, 0, -1, 3, 4);
	}
	
	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidPremiumSeatCapacity() throws AircraftException {
		//premium class seat capacity must be >= 0
		new A380("QF11", 5, 0, 0, -1, 4);
	}
	
	@Test(expected = AircraftException.class)
	public void A380StringIntIntIntIntIntInvalidEconomySeatCapacity() throws AircraftException {
		//economy class seat must be >= 0
		new A380("QF11", 5, 0, 0, 0, -1);
	}
	
	@Test
	public void testA380StringIntIntIntIntInt() throws AircraftException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		assertTrue(plane.flightEmpty());
	}

	
	@Test(expected = AircraftException.class)
	public void testCancelBookingUnbookedPassenger() throws PassengerException, AircraftException {
		//passenger must be booked before cancellation
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.cancelBooking(p, 3);
	}
	
	@Test
	public void testCancelBooking() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		plane.cancelBooking(p, 3);
		assertTrue(plane.getNumEconomy() == 0);
	}


	
	@Test(expected = AircraftException.class)
	public void testConfirmBookingNoSeatsAvailable() throws AircraftException, PassengerException {
		//seats must be available before booking
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		First p1 = new First(1, 5);
		First p2 = new First(1, 5);
		plane.confirmBooking(p1, 1);
		plane.confirmBooking(p2, 1);
	}
	
	@Test
	public void testConfirmBooking() throws PassengerException, AircraftException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 2);
		assertTrue(plane.getNumEconomy() == 1);
	}
	
	@Test
	public void testConfirmBooking2() throws PassengerException, AircraftException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p1 = new Economy(3, 5);
		
		plane.confirmBooking(p1, 4);
		
		System.out.println(plane.getNumEconomy() + " " + plane.getNumPremium() + " " + plane.getNumBusiness() + " " + plane.getNumFirst());

	}

	@Test
	public void testFlightEmpty() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		assertFalse(plane.flightEmpty());
	}

	@Test
	public void testFlightFull() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 1, 1, 1);
		
		Economy p1 = new Economy(1, 5);
		Premium p2 = new Premium(1, 5);
		Business p3 = new Business(1, 5);
		First p4 = new First(1, 5);
		
		plane.confirmBooking(p1, 1);
		plane.confirmBooking(p2, 1);
		plane.confirmBooking(p3, 1);
		plane.confirmBooking(p4, 1);
		
		assertFalse(plane.flightEmpty());
	}

	
	@Test
	public void testFlyPassengers() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		plane.flyPassengers(4);
		assertTrue(p.isFlown());
	}

	@Test
	public void testGetBookings() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		Bookings bookings = plane.getBookings();
		assertTrue(bookings.getNumEconomy() == 1);
	}

	@Test
	public void testGetPassengers() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		assertTrue(plane.getPassengers().size() == 1);
	}

	@Test
	public void testSeatsAvailable() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		assertTrue(plane.seatsAvailable(p));
	}
	
	@Test
	public void testUpgradeBookings() throws AircraftException, PassengerException {
		A380 plane = new A380("QF11", 5, 1, 2, 3, 4);
		Economy p = new Economy(3, 4);
		plane.confirmBooking(p, 3);
		plane.upgradeBookings();
		assertTrue(plane.getNumPremium() == 1);
	}

}
