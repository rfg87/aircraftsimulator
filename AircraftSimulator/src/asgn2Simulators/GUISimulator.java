/**
 *
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 *
 */
package asgn2Simulators;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;
import asgn2Passengers.PassengerException;

/**
 * @author hogan
 * @author Bradley Gray
 * @author Ron Grande
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements Runnable {
	
	private JFrame frame;
	private Container pane;
	private XYSeriesCollection lineChartData;
	private JFreeChart chart;
	private JFreeChart barChart;
	private static final String TITLE = "Aircraft Simulator";
	private DefaultCategoryDataset barChartData = new DefaultCategoryDataset();
	
	private int defaultSeed = Constants.DEFAULT_SEED;
	private double defaultBookingMean = Constants.DEFAULT_DAILY_BOOKING_MEAN;
	private double defaultBookingSD = 0.33;
	private int defaultMaxQueueSize = Constants.DEFAULT_MAX_QUEUE_SIZE;
	private double defaultCancellationProb = Constants.DEFAULT_CANCELLATION_PROB;
	private double defaultFirstProb = Constants.DEFAULT_FIRST_PROB;
	private double defaultBusinessProb = Constants.DEFAULT_BUSINESS_PROB;
	private double defaultPremiumProb = Constants.DEFAULT_PREMIUM_PROB;
	private double defaultEconomyProb = Constants.DEFAULT_ECONOMY_PROB;

	/**
	 * GUI Simulator with new input values
	 * @param args
	 * @throws HeadlessException
	 */
	public GUISimulator(String[] args) throws HeadlessException {
		super(TITLE);
		// Setup input values
		this.defaultSeed = Integer.parseInt(args[0]);
		this.defaultMaxQueueSize = Integer.parseInt(args[1]);
		this.defaultBookingMean = Double.parseDouble(args[2]);
		this.defaultBookingSD = Double.parseDouble(args[2]);
		this.defaultFirstProb = Double.parseDouble(args[4]);
		this.defaultBusinessProb = Double.parseDouble(args[5]);
		this.defaultPremiumProb = Double.parseDouble(args[6]);
		this.defaultEconomyProb = Double.parseDouble(args[7]);
		this.defaultCancellationProb = Double.parseDouble(args[8]);
	}
	
	/**
	 * GUI simulator with default input values
	 * @param args
	 * @throws HeadlessException
	 */
	public GUISimulator() throws HeadlessException {
		super(TITLE);
	}

	/**
	 * Method to add all GUI components to the pane
	 *
	 * @author Bradley Gray
	 */
	private void addComponentsToPane() {
		// Init component variables
		JFormattedTextField fTextField;
		JLabel label;
		GridBagConstraints c = new GridBagConstraints();
		final org.jfree.chart.ChartPanel panel = new org.jfree.chart.ChartPanel(chart); // with empty chart
		final org.jfree.chart.ChartPanel barPanel = new org.jfree.chart.ChartPanel(barChart); // with empty chart

		// Set Layout
		pane.setLayout(new GridBagLayout());

		// Buttons		
		// Show chart button
		final JButton chartButton = new JButton("Show Line Chart");
		chartButton.addActionListener(new ActionListener() {
			// Add button action
			@Override
			public void actionPerformed(ActionEvent e) {
				// Show Chart
				if (chartButton.getText().equals("Show Line Chart")) {
					// Toggle the text/bar chart area and the chart area
					pane.getComponent(2).setVisible(false);
					pane.getComponent(22).setVisible(true);
					pane.getComponent(23).setVisible(false);
					
					// Fill chart with correct data
					chart = createChart(lineChartData);
					panel.setChart(chart);
					
					// Change button text
					chartButton.setText("Show Bar Chart");
				// Show Bar Chart
				} else if (chartButton.getText().equals("Show Bar Chart")) {
					// Toggle the text area and the bar chart area
					pane.getComponent(2).setVisible(false);
					pane.getComponent(22).setVisible(false);
					pane.getComponent(23).setVisible(true);
					
					// Fill chart with correct data
					barChart = createBarChart(barChartData);
					barPanel.setChart(barChart);
					
					// Change button text
					chartButton.setText("Show Log");										
				// Show Log
				} else if (chartButton.getText().equals("Show Log")) {
					// Toggle the text area and the chart area
					pane.getComponent(2).setVisible(true);
					pane.getComponent(22).setVisible(false);
					pane.getComponent(23).setVisible(false);
					
					// Change button text
					chartButton.setText("Show Line Chart");
				}
			}
			
			// Method to setup the chart to be displayed
			// Takes a XYdataset as an argument and returns a Time series chart containing that data
			private JFreeChart createBarChart(DefaultCategoryDataset data) {
				final JFreeChart barChart = ChartFactory.createBarChart("Summary", "Category", "Value", data);
				return barChart;
			}

			// Method to setup the chart to be displayed
			// Takes a XYdataset as an argument and returns a Time series chart containing that data
			private JFreeChart createChart(final XYDataset dataset) {
				// Create chart
		        final JFreeChart result = ChartFactory.createXYLineChart("Daily Bookings", "Day", "Passengers", dataset, PlotOrientation.VERTICAL, true, true, false);
		        
		        // Modify plot axis to be automatic
		        final XYPlot plot = result.getXYPlot();
		        plot.getDomainAxis().setAutoRange(true);
		        plot.getRangeAxis().setAutoRange(true);
		        
		        return result;
			}
		});
		// Disable button initially
		chartButton.setEnabled(false);
		c.gridx = 4;
		c.gridheight = 2;
		c.gridwidth = 2;
		c.ipadx = 3;
		c.ipady = 2;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5,10,5,10);
		c.gridy = 13;
		pane.add(chartButton, c);
		
		// Start Simulation Button
		final JButton startButton = new JButton("Run Simulation");
		startButton.addActionListener(new ActionListener() {
			// Add button action
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Run Simulation
				if (startButton.getText().equals("Run Simulation")) {
					// Check if inputs are correct
					String str = checkInputs();
					setText("");
					
					// Show text area, hide graph area
					pane.getComponent(22).setVisible(false);
					pane.getComponent(2).setVisible(true);
					
					// If all inputs are correct
					if (str.equals("true")) {
						// Clear text area and setup GUI
						setText("");
						
						// Enable chart button
						chartButton.setEnabled(true);
						startButton.setText("Reset Simulation");
						
						try {
							// Attempt to create simulation
							createSimulation(obtainValues());
						} catch (SimulationException | IOException | AircraftException | PassengerException e) {
							// Catch any exception, output this and exit program
							e.printStackTrace();
							System.exit(-1);
						}
					} else {
						// If there is an error in the input, display this error in text area
						setText(str);
					}
				// Reset Simulation
				} else if (startButton.getText().equals("Reset Simulation")) {
					// Show text area, hide graph area
					pane.getComponent(22).setVisible(false);
					pane.getComponent(23).setVisible(false);
					pane.getComponent(2).setVisible(true);
					
					// Reset text area
					setText("");
					
					// Disable Chart Button
					chartButton.setText("Show Line Chart");
					chartButton.setEnabled(false);
					
					// Change button text
					startButton.setText("Run Simulation");
				}
			}
		});
		c.gridx = 4;
		c.gridy = 11;
		c.gridheight = 2;
		c.gridwidth = 2;
		c.ipadx = 3;
		c.ipady = 2;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(5,10,5,10);
		pane.add(startButton, c);
		
		// Main Frame
		// Main text output area for the simulation
		// Placed inside scroll area
		JTextArea logArea = new JTextArea(10, 20);
		logArea.setEditable(false);
		logArea.setLineWrap(true);
		logArea.setWrapStyleWord(true);
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.ipadx = 5;
		c.ipady = 5;
		c.gridwidth = 6;
		c.gridheight = 10;
		c.insets = new Insets(10,10,10,10);
		c.fill = GridBagConstraints.BOTH;
		JScrollPane scroll = new JScrollPane(logArea);
		// Always display scrollbar
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pane.add(scroll, c);

		// Text Fields
		// User input areas for simulation parameters
		fTextField = new JFormattedTextField(defaultSeed);
		fTextField.setColumns(6);
		c.weightx = 0.16;
		c.weighty = 0.01;
		c.gridx = 1;
		c.gridy = 11;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.ipadx = 5;
		c.ipady = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(3,5,3,5);
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultBookingMean);
		fTextField.setColumns(6);
		c.gridy = 12;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultMaxQueueSize);
		fTextField.setColumns(6);
		c.gridy = 13;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultCancellationProb);
		fTextField.setColumns(6);
		c.gridy = 14;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultFirstProb);
		fTextField.setColumns(6);
		c.gridx = 3;
		c.gridy = 11;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultBusinessProb);
		fTextField.setColumns(6);
		c.gridy = 12;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultPremiumProb);
		fTextField.setColumns(6);
		c.gridy = 13;
		pane.add(fTextField, c);

		fTextField = new JFormattedTextField(defaultEconomyProb);
		fTextField.setColumns(6);
		c.gridy = 14;
		pane.add(fTextField, c);

		// Labels
		// Display labels for describing text fields
		label = new JLabel();
		label.setText("RNG Seed");
		c.gridx = 0;
		c.gridy = 11;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.ipadx = 5;
		c.ipady = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Daily Mean");
		c.gridy = 12;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Queue Size");
		c.gridy = 13;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Cancellation");
		c.gridy = 14;
		pane.add(label, c);

		label = new JLabel();
		label.setText("First");
		c.gridx = 2;
		c.gridy = 11;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Business");
		c.gridy = 12;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Premium");
		c.gridy = 13;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Economy");
		c.gridy = 14;
		pane.add(label, c);

		// Big Labels
		// Category labels
		label = new JLabel();
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setText("Simulation");
		c.gridx = 0;
		c.gridy = 10;
		c.gridwidth = 2;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Fare Classes");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		c.gridx = 2;
		pane.add(label, c);

		label = new JLabel();
		label.setText("Operation");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		c.gridx = 4;
		c.anchor = GridBagConstraints.PAGE_START;
		pane.add(label, c);
		
		// Chart Frame
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.ipadx = 5;
		c.ipady = 5;
		c.gridwidth = 6;
		c.gridheight = 10;
		c.insets = new Insets(10,10,10,10);
		c.fill = GridBagConstraints.BOTH;
		pane.add(panel, c);
		
		// Bar Chart Frame
		pane.add(barPanel, c);
	}
	
	/*
	 * Indexes of pane items
	 * Buttons = 0 -> 1
	 * Text Field = 2
	 * Formatted Text Field = 3 -> 10
	 * Labels = 11 -> 18
	 * Big Labels = 19 -> 21
	 * Chart Panel = 22
	 * Bar Chart Panel = 23
	 */
	
	/**
	 * Helper method to obtain all values from the inputs in the GUI
	 *
	 * @return <code>String[]</code> containing the text inputs, without commas
	 *
	 * @author Bradley Gray
	 */
	public String[] obtainValues() {
		// Obtain components
		Component[] comps = pane.getComponents();
		String[] args = new String[9];

		// Obtain individual text inputs
		args[0] = ((JFormattedTextField)comps[3]).getText().replaceAll(",", ""); // seed
		args[1] = ((JFormattedTextField)comps[5]).getText().replaceAll(",", ""); // max queue size
		args[2] = ((JFormattedTextField)comps[4]).getText().replaceAll(",", ""); // mean bookings
		// SD of bookings is proportional to Mean Bookings
		args[3] = Double.toString(Double.parseDouble(args[2])*defaultBookingSD); // sd bookings
		args[4] = ((JFormattedTextField)comps[7]).getText().replaceAll(",", ""); // first prob
		args[5] = ((JFormattedTextField)comps[8]).getText().replaceAll(",", ""); // business prob
		args[6] = ((JFormattedTextField)comps[9]).getText().replaceAll(",", ""); // premium prob
		args[7] = ((JFormattedTextField)comps[10]).getText().replaceAll(",", ""); // economy prob
		args[8] = ((JFormattedTextField)comps[6]).getText().replaceAll(",", ""); // cancel prob
		// Return string array
		return args;
	}

	/**
	 * Helper method to set the text in the main logging area of the GUI
	 *
	 * @param str <code>String</code> that will be inserted into the text area
	 *
	 * @author Bradley Gray
	 */
	public void setText(String str) {
		// Obtain text area component
		JTextArea logArea = (JTextArea) ((JViewport)((JScrollPane)pane.getComponent(2)).getViewport()).getView();
		// Set text
		logArea.setText(str);
	}

	/**
	 * Helper method to add text in the main logging area of the GUI
	 *
	 * @param str <code>String</code> that will be appended to the end of the text area
	 *
	 * @author Bradley Gray
	 */
	public void addText(String str) {
		// Obtain text area component
		JTextArea logArea = (JTextArea) ((JViewport)((JScrollPane)pane.getComponent(2)).getViewport()).getView();
		// Add text
		logArea.setText(logArea.getText() + str);
	}

	/**
	 * Helper method to check inputs for the simulation and determine whether
	 * all inputs are within valid ranges
	 * 
	 * @return <code>String</code> containing either the error message to return
	 * 		   or "true"
	 *
	 * @author Bradley Gray
	 */
	private String checkInputs() {
		// Obtain values of inputs
		String[] args = obtainValues();
		String str = "Error in Inputs: ";

		// Check seed value
		if (Integer.parseInt(args[0]) < 0) {
			return str + "Seed Value cannot be lower than 0";
			// Check max queue size
		} else if (Integer.parseInt(args[1]) < 0) {
			return str + "Max Queue Size cannot be lower than 0";
			// Check daily booking mean
		} else if (Double.parseDouble(args[2]) < 0) {
			return str + "Daily Booking Mean cannot be lower than 0";
			// Check fare class probabilities
		} else if (Double.parseDouble(args[4]) + Double.parseDouble(args[5]) + Double.parseDouble(args[6]) + Double.parseDouble(args[7]) != 1.0) {
			return str + "Fare Class probabilities must add to 1.0";
			// Check cancellation probability
		} else if (Double.parseDouble(args[8]) > 1 || Double.parseDouble(args[8]) < 0) {
			return str + "Cancellation probability must be between 0.0 and 1.0";
			// Else return true
		} else {
			return "true";
		}
	}
	
	/**
	 * Method to create the dataset from given data create from the simulation
	 * Each parameter is an array containing all the passenger bookings for each fare class
	 * 
	 * @param numEconomy
	 * @param numBusiness
	 * @param numPremium
	 * @param numFirst
	 * @param numAvailable = number of availabile bookings
	 * @param numTotal = total number of bookings made
	 * 
	 * @author Bradley Gray
	 */
	private void createDataset(int[] numEconomy, int[] numBusiness, int[] numPremium, 
			int[] numFirst,	int[] numAvailable, int[] numTotal) {
		// Reset dataset
		lineChartData = new XYSeriesCollection();
		
		// Init XYSeries'
		XYSeries economyTotal = new XYSeries("Economy");
		XYSeries businessTotal = new XYSeries("Business");
		XYSeries premiumTotal = new XYSeries("Premium");
		XYSeries firstTotal = new XYSeries("First");
		XYSeries totalTotal = new XYSeries("Total");
		XYSeries availableTotal = new XYSeries("Available");
		
		// For each value in the arrays add them to the corresponding XYSeries
		for (int i = 0; i < numEconomy.length; i++) {
			economyTotal.add(i, numEconomy[i]);
			businessTotal.add(i, numBusiness[i]);
			premiumTotal.add(i, numPremium[i]);
			firstTotal.add(i, numFirst[i]);
			totalTotal.add(i, numTotal[i]);
			availableTotal.add(i, numAvailable[i]);
		}
		
		// Add the XYseries to the dataset
		lineChartData.addSeries(totalTotal);
		lineChartData.addSeries(economyTotal);
		lineChartData.addSeries(premiumTotal);
		lineChartData.addSeries(businessTotal);
		lineChartData.addSeries(firstTotal);
		lineChartData.addSeries(availableTotal);
	}
	
	/**
	 * Helper method to create data set for summary bar chart
	 * 
	 * @param queue = size of largest queue in simulation
	 * @param refused = total number of refused passengers in simulation
	 * @param capacity = total of flight capacities in simulation
	 * 
	 * @author Ron Grande
	 */
	private void createBarChartDataset(int queue, int refused, int capacity)
	{
		// Axis titles
		final String maxQueue = "Queue";
		final String refusedPassengers = "Refused";
		final String totalCapacity = "Capacity";
		final String p = "Passengers";
		
		// Add data to bar chart
		barChartData.addValue(queue, p, maxQueue);
		barChartData.addValue(refused, p, refusedPassengers);
		barChartData.addValue(capacity, p, totalCapacity);
	}

	/**
	 * Method to create and run the simulation
	 * 
	 * @param args <code>String []</code> containing all the arguments to give to the simulator
	 * Exceptions propograted upwards
	 * @throws SimulationException
	 * @throws IOException
	 * @throws AircraftException
	 * @throws PassengerException
	 *
	 * @author Bradley Gray
	 */
	private void createSimulation(String[] args) throws SimulationException, IOException, AircraftException, PassengerException {
		// Obtain individual arguments
		int seed = Integer.parseInt(args[0]);
		int maxQueueSize = Integer.parseInt(args[1]);
		double meanBookings = Double.parseDouble(args[2]);
		double sdBookings = Double.parseDouble(args[3]);
		double firstProb = Double.parseDouble(args[4]);
		double businessProb = Double.parseDouble(args[5]);
		double premiumProb = Double.parseDouble(args[6]);
		double economyProb = Double.parseDouble(args[7]);
		double cancelProb = Double.parseDouble(args[8]);

		// Create new simulator with args
		Simulator sim = new Simulator(seed, maxQueueSize, meanBookings, sdBookings, firstProb, businessProb, premiumProb, economyProb, cancelProb);
		// Create new log
		Log log = new Log();
		
		// Run simulation
		runSimulation(sim, log);
	}

	/**
	 * Method to run the simulation from start to finish. 
	 * Exceptions are propagated upwards as necessary 
	 * Taken from the SimulationRunner class
	 * Modified to add text into the log area and save values ready for charting
	 *
	 * @param sim <code>Simulator</code> to be ran
	 * @param log <code>Log</code> to be written to
	 * @throws AircraftException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws PassengerException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws SimulationException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws IOException on logging failures See methods from {@link asgn2Simulators.Log} 
	 *
	 * @author hogan
	 * @author Bradley Gray
	 * @author Ron Grande
	 */
	private void runSimulation(Simulator sim, Log log) throws AircraftException, PassengerException, SimulationException, IOException {
		// Passenger Storage Variables for charting
		int duration = Constants.DURATION - Constants.FIRST_FLIGHT + 1;
		int[] numEconomy = new int[duration];
		int[] numBusiness = new int[duration];
		int[] numFirst = new int[duration];
		int[] numPremium = new int[duration];
		int[] numTotal = new int[duration];
		int[] numAvailable = new int[duration];
		//Variables for bar chart
		int maxQueue = 0;
		
		// Initialise simulation and logger
		String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		sim.createSchedule();
		log.initialEntry(sim);
		addText(timeLog + ": Start of Simulation\n");
		addText(sim.toString() + "\n");

		// Main simulation loop 
		for (int time=0; time<=Constants.DURATION; time++) {
			// Create new passengers, process cancellations
			sim.resetStatus(time);
			sim.rebookCancelledPassengers(time);
			sim.generateAndHandleBookings(time);
			sim.processNewCancellations(time);
			
			if (time >= Constants.FIRST_FLIGHT) {
				// Past the first flight fly passengers
				sim.processUpgrades(time);
				sim.processQueue(time);
				sim.flyPassengers(time);
				sim.updateTotalCounts(time);
				log.logFlightEntries(time, sim);
				
				// Output text summary				
				addText(sim.getSummary(time, true));
				
				// Log passenger counts for charting
				Bookings counts = sim.getFlights(time).getCurrentCounts();
				int t = time - Constants.FIRST_FLIGHT;
				
				numEconomy[t] = counts.getNumEconomy();
				numBusiness[t] = counts.getNumBusiness();
				numFirst[t] = counts.getNumFirst();
				numPremium[t] = counts.getNumPremium();
				numTotal[t] = counts.getTotal();
				numAvailable[t] = counts.getAvailable();
			} else {
				// Process queue
				sim.processQueue(time);			
				// Output text summary
				addText(sim.getSummary(time, false));
			}
			
			// Log progress
			log.logQREntries(time, sim);
			log.logEntry(time, sim);
			
			// If number in queue greater than currently stored maxQueue
			if(sim.numInQueue() > maxQueue) {
				maxQueue = sim.numInQueue();
			}
		
		}
		// Finalise simulation
		sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION); 
		log.logQREntries(Constants.DURATION, sim);
		log.finalise(sim);
		// Add final text
		addText(sim.finalState());
		
		// Setup dataset
		createDataset(numEconomy, numBusiness, numPremium, numFirst, numAvailable, numTotal);
		//Setup bar chart data set
		createBarChartDataset(maxQueue, sim.numRefused(), sim.getTotalFlown());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 * 
	 * @author Bradley Gray
	 */
	@Override
	public void run() {
		// Create and set up the window
		JFrame.setDefaultLookAndFeelDecorated(true);
		this.frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(1000, 600));
		frame.setLocation(new Point(200, 200));
		
		// Store Pane
		this.pane = frame.getContentPane();

		// Add components to window
		addComponentsToPane();

		// Display the Window
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}
}