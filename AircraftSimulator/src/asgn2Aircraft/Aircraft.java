/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Aircraft;


import java.util.ArrayList;
import java.util.List;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;
import asgn2Simulators.Log;

/**
 * The <code>Aircraft</code> class provides facilities for modelling a commercial jet 
 * aircraft with multiple travel classes. New aircraft types are created by explicitly 
 * extending this class and providing the necessary configuration information. 
 * 
 * In particular, <code>Aircraft</code> maintains a collection of currently booked passengers, 
 * those with a Confirmed seat on the flight. Queueing and Refused bookings are handled by the 
 * main {@link asgn2Simulators.Simulator} class. 
 *   
 * The class maintains a variety of constraints on passengers, bookings and movement 
 * between travel classes, and relies heavily on the asgn2Passengers hierarchy. Reports are 
 * also provided for logging and graphical display. 
 * 
 * @author hogan
 * @author Bradley Gray
 *
 */
public abstract class Aircraft {

	protected int firstCapacity;
	protected int businessCapacity;
	protected int premiumCapacity;
	protected int economyCapacity;
	protected int capacity;
		
	protected int numFirst;
	protected int numBusiness;
	protected int numPremium; 
	protected int numEconomy; 

	protected String flightCode;
	protected String type; 
	protected int departureTime; 
	protected String status;
	protected List<Passenger> seats;

	/**
	 * Constructor sets flight info and the basic size parameters. 
	 * 
	 * @param flightCode <code>String</code> containing flight ID 
	 * @param departureTime <code>int</code> scheduled departure time
	 * @param first <code>int</code> capacity of First Class 
	 * @param business <code>int</code> capacity of Business Class 
	 * @param premium <code>int</code> capacity of Premium Economy Class 
	 * @param economy <code>int</code> capacity of Economy Class 
	 * @throws AircraftException if isNull(flightCode) OR (departureTime <=0) OR ({first,business,premium,economy} <0)
	 */
	public Aircraft(String flightCode,int departureTime, int first, int business, int premium, int economy) throws AircraftException {
		// Check pre-conditions
		if (flightCode == null) {
			throw new AircraftException("Cannot create aircraft: flightCode must be non-null");
		} else if (departureTime <=0) {
			throw new AircraftException("Cannot create aircraft: departureTime must be greater than 0");
		} else if (first < 0) {
			throw new AircraftException("Cannot create aircraft: Seat capacity for First class must be greater than 0");
		} else if (business < 0) {
			throw new AircraftException("Cannot create aircraft: Seat capacity for Business class must be greater than 0");
		} else if (premium < 0) {
			throw new AircraftException("Cannot create aircraft: Seat capacity for Premium class must be greater than 0");
		} else if (economy < 0) {
			throw new AircraftException("Cannot create aircraft: Seat capacity for Economy class must be greater than 0");
		} else {
	    // Create Aircraft variables
			this.flightCode = flightCode;
			this.departureTime = departureTime;
			this.firstCapacity = first;
			this.businessCapacity = business;
			this.premiumCapacity = premium;
			this.economyCapacity = economy;
			this.status = "";
			this.seats = new ArrayList<Passenger>();
			this.numBusiness = 0;
			this.numEconomy = 0;
			this.numFirst = 0;
			this.numPremium = 0;
			this.capacity = first + business + premium + economy;
		}
	}
	
	/**
	 * Method to remove passenger from the aircraft - passenger must have a confirmed 
	 * seat prior to entry to this method.   
	 *
	 * @param p <code>Passenger</code> to be removed from the aircraft 
	 * @param cancellationTime <code>int</code> time operation performed 
	 * @throws PassengerException if <code>Passenger</code> is not Confirmed OR cancellationTime 
	 * is invalid. See {@link asgn2Passengers.Passenger#cancelSeat(int)}
	 * @throws AircraftException if <code>Passenger</code> is not recorded in aircraft seating 
	 */
	public void cancelBooking(Passenger p,int cancellationTime) throws PassengerException, AircraftException {
		// Check passenger has a confirmed seat
		if (getPassengers().contains(p) == false) {
			throw new AircraftException("Cannot cancel booking: Passenger is not recorded in aircraft seating");
		} else {
			// Transition passenger
			p.cancelSeat(cancellationTime);
			// Update status string
			this.status += Log.setPassengerMsg(p,"C","N");
			// Remove passenger from seat storage
			this.seats.remove(p);
			// Lower the passenger count
			switch (getPassengerClass(p)) {
				case 0: this.numEconomy--; break;
				case 1: this.numPremium--; break;
				case 2: this.numBusiness--; break;
				case 3: this.numFirst--; break;
			}
		}
	}

	/**
	 * Method to add a Passenger to the aircraft seating. 
	 * Precondition is a test that a seat is available in the required fare class
	 * 
	 * @param p <code>Passenger</code> to be added to the aircraft 
	 * @param confirmationTime <code>int</code> time operation performed 
	 * @throws PassengerException if <code>Passenger</code> is in incorrect state 
	 * OR confirmationTime OR departureTime is invalid. See {@link asgn2Passengers.Passenger#confirmSeat(int, int)}
	 * @throws AircraftException if no seats available in <code>Passenger</code> fare class. 
	 */
	public void confirmBooking(Passenger p,int confirmationTime) throws AircraftException, PassengerException { 
		// Check if seats available
		if (!seatsAvailable(p)) {
			throw new AircraftException(noSeatsAvailableMsg(p));
		} else {
			// Confirm passenger and add them to the aircraft seats
			p.confirmSeat(confirmationTime, this.departureTime);
			// Update status string
			this.status += Log.setPassengerMsg(p,"N/Q","C");
			// Add passenger to seat storage
			this.seats.add(p);
			// Increment the passenger count
			System.out.println(getPassengerClass(p));
			switch (getPassengerClass(p)) {
				case 0: this.numEconomy++; break;
				case 1: this.numPremium++; break;
				case 2: this.numBusiness++; break;
				case 3: this.numFirst++; break; 
			}
			
		}
	}
	
	/**
	 * State dump intended for use in logging the final state of the aircraft. (Supplied) 
	 * 
	 * @return <code>String</code> containing dump of final aircraft state 
	 */
	public String finalState() {
		String str = aircraftIDString() + " Pass: " + this.seats.size() + "\n";
		for (Passenger p : this.seats) {
			str += p.toString() + "\n";
		}
		return str + "\n";
	}
	
	/**
	 * Simple status showing whether aircraft is empty
	 * 
	 * @return <code>boolean</code> true if aircraft empty; false otherwise 
	 */
	public boolean flightEmpty() {
		return this.seats.isEmpty();
	}
	
	/**
	 * Simple status showing whether aircraft is full
	 * 
	 * @return <code>boolean</code> true if aircraft full; false otherwise 
	 */
	public boolean flightFull() {
		return this.capacity - this.seats.size() <= 0;
	}
	
	/**
	 * Method to finalise the aircraft seating on departure. 
	 * Effect is to change the state of each passenger to Flown. 
	 * departureTime parameter allows for rescheduling 
	 * 
	 * @param departureTime <code>int</code> actual departureTime from simulation  
	 * @throws PassengerException if <code>Passenger</code> is in incorrect state 
	 * See {@link asgn2Passengers.Passenger#flyPassenger(int)}. 
	 */
	public void flyPassengers(int departureTime) throws PassengerException { 
		for (Passenger p: this.seats) {
			p.flyPassenger(departureTime);
		}
	}
	
	/**
	 * Method to return an {@link asgn2Aircraft.Bookings} object containing the Confirmed 
	 * booking status for this aircraft. 
	 * 
	 * @return <code>Bookings</code> object containing the status.  
	 */
	public Bookings getBookings() {
		return new Bookings(this.numFirst, this.numBusiness, this.numPremium, this.numEconomy, this.seats.size(), this.capacity - this.seats.size());
	}
		
	/**
	 * Simple getter for number of confirmed Business Class passengers
	 * 
	 * @return <code>int</code> number of Business Class passengers 
	 */
	public int getNumBusiness() {
		return this.numBusiness;
	}
	
	
	/**
	 * Simple getter for number of confirmed Economy passengers
	 * 
	 * @return <code>int</code> number of Economy Class passengers 
	 */
	public int getNumEconomy() {
		return this.numEconomy;
	}

	/**
	 * Simple getter for number of confirmed First Class passengers
	 * 
	 * @return <code>int</code> number of First Class passengers 
	 */
	public int getNumFirst() {
		return this.numFirst;
	}

	/**
	 * Simple getter for the total number of confirmed passengers 
	 * 
	 * @return <code>int</code> number of Confirmed passengers 
	 */
	public int getNumPassengers() {
		return this.seats.size();
	}
	
	/**
	 * Simple getter for number of confirmed Premium Economy passengers
	 * 
	 * @return <code>int</code> number of Premium Economy Class passengers
	 */
	public int getNumPremium() {
		return this.numPremium;
	}
	
	/**
	 * Method to return an {@link java.util.List} object containing a copy of 
	 * the list of passengers on this aircraft. 
	 * 
	 * @return <code>List<Passenger></code> object containing the passengers.  
	 */
	public List<Passenger> getPassengers() {
		// Copy list of passengers
		List<Passenger> passengers = new ArrayList<Passenger>(this.seats);
		return passengers;
	}
	
	/**
	 * Method used to provide the current status of the aircraft for logging. (Supplied) 
	 * Uses private status <code>String</code>, set whenever a transition occurs. 
	 *  
	 * @return <code>String</code> containing current aircraft state 
	 */
	public String getStatus(int time) {
		String str = time +"::"
		+ this.seats.size() + "::"
		+ "F:" + this.numFirst + "::J:" + this.numBusiness 
		+ "::P:" + this.numPremium + "::Y:" + this.numEconomy; 
		str += this.status;
		this.status="";
		return str+"\n";
	}
	
	/**
	 * Simple boolean to check whether a passenger is included on the aircraft 
	 * 
	 * @param p <code>Passenger</code> whose presence we are checking
	 * @return <code>boolean</code> true if isConfirmed(p); false otherwise 
	 */
	public boolean hasPassenger(Passenger p) {
		return p.isConfirmed();
	}
	
	/**
	 * State dump intended for logging the aircraft parameters (Supplied) 
	 * 
	 * @return <code>String</code> containing dump of initial aircraft parameters 
	 */ 
	public String initialState() {
		return aircraftIDString() + " Capacity: " + this.capacity 
				+ " [F: " 
				+ this.firstCapacity + " J: " + this.businessCapacity 
				+ " P: " + this.premiumCapacity + " Y: " + this.economyCapacity
				+ "]";
	}
	
	/**
	 * Given a Passenger, method determines whether there are seats available in that 
	 * fare class. 
	 *   
	 * @param p <code>Passenger</code> to be Confirmed
	 * @return <code>boolean</code> true if seats in Class(p); false otherwise
	 */
	public boolean seatsAvailable(Passenger p) {		
		// Switch passenger's class
		switch (getPassengerClass(p)) {
			case 0: return this.economyCapacity - this.numEconomy > 0;
			case 1: return this.premiumCapacity - this.numPremium > 0;
			case 2: return this.businessCapacity - this.numBusiness > 0;
			case 3: return this.firstCapacity - this.numFirst > 0;
		}
		// If passengers class is invalid return false
		return false;
	}
	
	/**
	 * Helper method that returns an integer value representation of the given passengers class
	 * 0 = Economy
	 * 1 = Premium
	 * 2 = Business
	 * 3 = First
	 * 
	 * @param p <code>Passenger</code> whose class we are checking
	 * @return <code>int</code> determining the class of the passenger
	 */
	private int getPassengerClass(Passenger p) {
		// Check what class the passenger is an instance of
		if (p instanceof Economy) {
			return 0;
		} else if (p instanceof Premium) {
			return 1;
		} else if (p instanceof Business) {
			return 2;
		} else if (p instanceof First) {
			return 3;
		}
		// If passengers class is invalid, return -1
		return -1;
	}

	/* 
	 * (non-Javadoc) (Supplied) 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return aircraftIDString() + " Count: " + this.seats.size() 
				+ " [F: " + numFirst
				+ " J: " + numBusiness 
				+ " P: " + numPremium 
				+ " Y: " + numEconomy 
			    + "]";
	}


	/**
	 * Method to upgrade Passengers to try to fill the aircraft seating. 
	 * Called at departureTime. Works through the aircraft fare classes in 
	 * descending order of status. No upgrades are possible from First, so 
	 * we consider Business passengers (upgrading if there is space in First), 
	 * then Premium, upgrading to fill spaces already available and those created 
	 * by upgrades to First), and then finally, we do the same for Economy, upgrading 
	 * where possible to Premium.  
	 */
	public void upgradeBookings() { 
		// Obtain list of passengers grouped by class
		List<Passenger> pBusiness = getPassengersByClass(2);
		List<Passenger> pPremium = getPassengersByClass(1);
		List<Passenger> pEconomy = getPassengersByClass(0);
		
		// Upgrade Business
		for (Passenger p: pBusiness) {
			if (this.firstCapacity - this.numFirst > 0) {
				// If there is space to upgrade, then upgrade passenger
				this.seats.remove(p);
				this.seats.add(p.upgrade());
				// Update Count
				this.numBusiness--;
				this.numFirst++;
			} else {
				// Stop loop if no available seats
				break;
			}
		}
		
		// Upgrade Premium
		for (Passenger p: pPremium) {
			if (this.businessCapacity - this.numBusiness > 0) {
				// If there is space to upgrade, then upgrade passenger
				this.seats.remove(p);
				this.seats.add(p.upgrade());
				// Update Count
				this.numPremium--;
				this.numBusiness++;
			} else {
				// Stop loop if no available seats
				break;
			}
		}	
		
		// Upgrade Economy
		for (Passenger p: pEconomy) {
			if (this.premiumCapacity - this.numPremium > 0) {
				// If there is space to upgrade, then upgrade passenger
				this.seats.remove(p);
				this.seats.add(p.upgrade());
				// Update Count
				this.numEconomy--;
				this.numPremium++;
			} else {
				// Stop loop if no available seats
				break;
			}
		}
	}
	
	/**
	 * Helper method to obtain a list of confirmed passengers of a certain (numerical) fare class
	 * 0 = Economy
	 * 1 = Premium
	 * 2 = Business
	 * 3 = First
	 * 
	 * @param iclass <code>int</code> integer representation of fare class
	 * @return passengers <code>List<Passenger></code> a list of confirmed passengers who are 
	 * 		   of the given fare class
	 */
	private List<Passenger> getPassengersByClass(int iclass) {
		List<Passenger> passengers = new ArrayList<Passenger>();
		
		// Loop through passenger list
		for (Passenger p: this.seats) {
			int pclass = getPassengerClass(p);
			// If the passenger's fare class is the same as the desired class, add to list
			if (pclass == iclass) {
				passengers.add(p);
			}
		}
		
		// Return list of passengers of desired fare class
		return passengers;
	}

	/**
	 * Simple String method for the Aircraft ID 
	 * 
	 * @return <code>String</code> containing the Aircraft ID 
	 */
	private String aircraftIDString() {
		return this.type + ":" + this.flightCode + ":" + this.departureTime;
	}


	//Various private helper methods to check arguments and throw exceptions, to increment 
	//or decrement counts based on the class of the Passenger, and to get the number of seats 
	//available in a particular class


	//Used in the exception thrown when we can't confirm a passenger 
	/** 
	 * Helper method with error messages for failed bookings
	 * @param p Passenger seeking a confirmed seat
	 * @return msg string failure reason 
	 */
	private String noSeatsAvailableMsg(Passenger p) {
		String msg = "";
		return msg + p.noSeatsMsg(); 
	}
}